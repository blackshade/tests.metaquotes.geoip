﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Tests.Metaquotes.GeoIP.WebApi.Services;

namespace Tests.Metaquotes.GeoIP.WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class GeoIpController : ApiController
    {
        // GET /ip/location?ip=123.234.432.321
        [HttpGet, Route("ip/location")]
        public IHttpActionResult GetLocationByIp(string ip)
        {
            IPAddress ipAddress;
            if (!IPAddress.TryParse(ip, out ipAddress))
                return BadRequest("invalid ipV4 address");

            var bytes = ipAddress.GetAddressBytes().Reverse().ToArray();

            var location = GeoIpService.FindLocationByIp(BitConverter.ToUInt32(bytes, 0));
            if (location == null)
                return NotFound();

            return Ok(location);
        }

        // GET /city/locations?city=cit_Gbqw4
        [HttpGet, Route("city/locations")]
        public IHttpActionResult GetLocationByCity(string city)
        {
            var locations = GeoIpService.FindLocationsByCity(city);

            return Ok(locations);
        }

        // GET ip/ranges/?offset=5000&count=50
        [HttpGet, Route("ip/ranges")]
        public IHttpActionResult GetIpRanges(int offset = 0, int count = 10)
        {
            var ranges = GeoIpService.GetRangesPage(offset, count);

            return Ok(ranges);
        }
    }
}
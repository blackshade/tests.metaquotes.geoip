﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Tests.Metaquotes.GeoIP.WebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Конфигурация и службы веб-API
            config.EnableCors();

            // Маршруты веб-API
            config.MapHttpAttributeRoutes();

            // Оставляем только json сериализацию
            config.Formatters.Remove(config.Formatters.XmlFormatter);
        }
    }
}

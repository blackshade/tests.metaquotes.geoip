﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using Tests.Metaquotes.GeoIP.WebApi.Services;

namespace Tests.Metaquotes.GeoIP.WebApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            var stopWatch = System.Diagnostics.Stopwatch.StartNew();
            GeoIpService.LoadData(Server.MapPath("~/App_Data/geobase.dat"));
            stopWatch.Stop();
            System.Diagnostics.Debug.WriteLine("LoadData Time: {0}ms", stopWatch.ElapsedMilliseconds);
        }
    }
}

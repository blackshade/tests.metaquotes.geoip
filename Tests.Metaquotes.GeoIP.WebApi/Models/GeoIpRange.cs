﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tests.Metaquotes.GeoIP.WebApi.Models
{
    public class GeoIpRange
    {
        public string From { get; set; }

        public string To { get; set; }

        public GeoIpLocation Location { get; set; }
    }
}
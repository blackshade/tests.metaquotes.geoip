﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tests.Metaquotes.GeoIP.WebApi.Models
{
    public class GeoIpRangesPage
    {
        public int Offset { get; set; }

        public int Total { get; set; }

        public IEnumerable<GeoIpRange> Items { get; set; }
    }
}
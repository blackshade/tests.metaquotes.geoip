﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Tests.Metaquotes.GeoIP.WebApi.Models
{
    public class GeoIpLocation
    {
        public GeoIpLocation(byte[] data)
        {
            if (data == null)
                throw new ArgumentNullException(nameof(data));
            if (data.Length != 96)
                throw new ArgumentException("Location size must be equal 96", nameof(data));

            _data = data;
        }

        byte[] _data;

        /// <summary>
        /// название страны (случайная строка с префиксом "cou_")
        /// </summary>
        public string Country => Encoding.ASCII.GetString(_data, 0, 8).TrimEnd((char)0).TrimEnd();

        /// <summary>
        /// название области (случайная строка с префиксом "reg_")
        /// </summary>
        public string Region => Encoding.ASCII.GetString(_data, 8, 12).TrimEnd((char)0).TrimEnd();

        /// <summary>
        /// почтовый индекс (случайная строка с префиксом "pos_")
        /// </summary>
        public string Postal => Encoding.ASCII.GetString(_data, 20, 12).TrimEnd((char)0).TrimEnd();

        /// <summary>
        /// название города (случайная строка с префиксом "cit_")
        /// </summary>
        public string City => Encoding.ASCII.GetString(_data, 32, 24).TrimEnd((char)0).TrimEnd();

        /// <summary>
        /// название организации (случайная строка с префиксом "org_")
        /// </summary>
        public string Organization => Encoding.ASCII.GetString(_data, 56, 32).TrimEnd((char)0).TrimEnd();

        /// <summary>
        /// широта
        /// </summary>
        public float Latitude => BitConverter.ToSingle(_data, 88);

        /// <summary>
        /// долгота
        /// </summary>
        public float Longitude => BitConverter.ToSingle(_data, 92);
    }
}
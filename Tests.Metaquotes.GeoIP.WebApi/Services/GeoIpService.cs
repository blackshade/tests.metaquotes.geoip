﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Tests.Metaquotes.GeoIP.WebApi.Models;

namespace Tests.Metaquotes.GeoIP.WebApi.Services
{
    public static class GeoIpService
    {
        public class GeoIpHeader
        {
            public int version;           // версия база данных
            public string name;           // название/префикс для базы данных
            public ulong timestamp;       // время создания базы данных
            public int records;           // общее количество записей
            public uint offset_ranges;    // смещение относительно начала файла до начала списка записей с геоинформацией
            public uint offset_cities;    // смещение относительно начала файла до начала индекса с сортировкой по названию городов
            public uint offset_locations; // смещение относительно начала файла до начала списка записей о местоположении
        }

        static GeoIpHeader _header;

        static byte[] _database;

        public static void LoadData(string path)
        {
            _database = File.ReadAllBytes(path);

            _header = new GeoIpHeader
            {
                version = BitConverter.ToInt32(_database, 0),
                name = Encoding.ASCII.GetString(_database, 4, 32),
                timestamp = BitConverter.ToUInt64(_database, 36),
                records = BitConverter.ToInt32(_database, 44),
                offset_ranges = BitConverter.ToUInt32(_database, 48),
                offset_cities = BitConverter.ToUInt32(_database, 52),
                offset_locations = BitConverter.ToUInt32(_database, 56),
            };
        }

        public static GeoIpLocation FindLocationByIp(uint ip)
        {
            int left = 0;
            int right = _header.records - 1;

            while (left <= right)
            {
                var middle = left + (right - left) / 2;

                var offset = (int)_header.offset_ranges + 12 * middle;
                var ipStart = BitConverter.ToUInt32(_database, offset);
                var ipEnd = BitConverter.ToUInt32(_database, offset + 4);

                if (ipStart > ip)
                {
                    right = middle - 1;
                }
                else if (ipEnd < ip)
                {
                    left = middle + 1;
                }
                else
                {
                    var index = BitConverter.ToUInt32(_database, offset + 8);

                    return GetLocationByOffset(_header.offset_locations + 96 * index);
                }
            }

            return null;
        }

        private static GeoIpLocation GetLocationByOffset(uint offset)
        {
            var data = new byte[96];
            Array.Copy(_database, offset, data, 0, 96);
            return new GeoIpLocation(data);
        }

        public static IEnumerable<GeoIpLocation> FindLocationsByCity(string city)
        {
            var locations = new List<GeoIpLocation>();

            int left = 0;
            //int right = _header.records - 1;
            int right = (_database.Length - (int)_header.offset_cities) / 4 - 1;

            while (left <= right)
            {
                var middle = left + (right - left) / 2;

                var offset = (int)_header.offset_cities + 4 * middle;
                var locationIndex = BitConverter.ToUInt32(_database, offset);
                var locationOffset = _header.offset_locations + locationIndex;

                var currentCity = Encoding.ASCII.GetString(_database, (int)locationOffset + 32, 24).TrimEnd((char)0).TrimEnd();

                var result = string.CompareOrdinal(city, currentCity);

                if (result < 0)
                {
                    right = middle - 1;
                }
                else if (result > 0)
                {
                    left = middle + 1;
                }
                else
                {
                    locations.Add(GetLocationByOffset(locationOffset));

                    // нашли значение, но оно может быть не одно.
                    // Идем влево и вправо для поиска границ
                    var minOffset = offset;
                    while (minOffset >= _header.offset_cities)
                    {
                        minOffset -= 4;

                        locationIndex = BitConverter.ToUInt32(_database, minOffset);
                        var location = GetLocationByOffset(_header.offset_locations + locationIndex);
                        if (location.City != city)
                            break;

                        locations.Insert(0, location);
                    }

                    var maxOffset = offset;
                    while (maxOffset <= _database.Length - 4)
                    {
                        maxOffset += 4;

                        locationIndex = BitConverter.ToUInt32(_database, minOffset);
                        var location = GetLocationByOffset(_header.offset_locations + locationIndex);
                        if (location.City != city)
                            break;

                        locations.Add(location);
                    }

                    break;
                }
            }

            return locations;
        }

        public static GeoIpRangesPage GetRangesPage(int offset, int count)
        {
            var result = new List<GeoIpRange>();

            var max = offset + count;
            if (max > _header.records)
                max = _header.records;

            for (int i = offset; i < max; i++)
            {
                var rangeOffset = (int)_header.offset_ranges + 12 * i;

                var index = BitConverter.ToUInt32(_database, rangeOffset + 8);

                result.Add(new GeoIpRange
                {
                    From = string.Format("{0}.{1}.{2}.{3}", _database[rangeOffset + 3], _database[rangeOffset + 2], _database[rangeOffset + 1], _database[rangeOffset]),
                    To = string.Format("{0}.{1}.{2}.{3}", _database[rangeOffset + 7], _database[rangeOffset + 6], _database[rangeOffset + 5], _database[rangeOffset + 4]),
                    Location = GetLocationByOffset(_header.offset_locations + 96 * index),
                });
            }

            return new GeoIpRangesPage
            {
                Total = _header.records,
                Offset = offset,
                Items = result,
            };
        }
    }
}
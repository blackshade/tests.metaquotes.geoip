import Vue from 'vue';
import Router from 'vue-router';

import IpRanges from '@/views/IpRanges'
import LocationByIp from '@/views/LocationByIp'
import LocationsByCity from '@/views/LocationsByCity'

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/ip',
      name: 'Ip',
      component: LocationByIp,
    },
    {
      path: '/',
      name: 'Dashboard',
      redirect: {
        name: 'Ip',
      },
    },
    {
      path: '/city',
      name: 'City',
      component: LocationsByCity,
    },
    {
      path: '/ranges',
      name: 'Ranges',
      component: IpRanges,
    },
  ],
});

export default router;

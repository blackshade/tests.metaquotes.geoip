import { create, isCancel } from 'axios';

export const baseUrl = 'http://localhost:62848/';
const api = create({
  baseURL: baseUrl,
});

export default {
  ...api,
  isCancel,
  baseUrl,
};
